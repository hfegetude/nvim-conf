-- We have to set the leader key here for lazy.nvim to work
require("helpers.keys").set_leader(" ")

require("helpers.custom")
require("core.lazy")
