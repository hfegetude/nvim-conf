return {
	{
		"nvim-telescope/telescope.nvim",
		branch = "0.1.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
			-- Fuzzy Finder Algorithm which requires local dependencies to be built. Only load if `make` is available
			{ "nvim-telescope/telescope-fzf-native.nvim", build = "make", cond = vim.fn.executable("make") == 1 },
		},

		config = function()
			local map = require("helpers.keys").map
			map("n", "<leader>ff", require("telescope.builtin").find_files, "Files")
			map("n", "<leader>fb", require("telescope.builtin").buffers, "Buffers")
			map("n", "<leader>fg", require("telescope.builtin").live_grep, "RipGrep")
			map("n", "<leader>fs", require("telescope.builtin").grep_string, "Grep String")
			map("n", "<leader>fr", require("telescope.builtin").lsp_references, "LSP references")
			map("n", "<leader>fc", "<CMD>Telescope neoclip<CR>", "neoclip")
		end

	}
}
