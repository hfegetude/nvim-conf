return {
	{
		"numToStr/Comment.nvim",
		lazy = false,
		config = function ()
			require("Comment").setup()
		end
	},
	{
		"ggandor/leap.nvim",
		config = function()
			require("leap").create_default_mappings()
		end
	},
	{
  		'stevearc/oil.nvim',
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function ()
			require("oil").setup()
			local map = require("helpers.keys").map
			map("n", "<leader>o", "<CMD>Oil --float<CR>", "Open OIL")
		end
	},
	{
		"AckslD/nvim-neoclip.lua",
		requires = {
    			{'kkharji/sqlite.lua', module = 'sqlite'},
  		},
  		config = function()
    			require('neoclip').setup({
			})
  		end,
	}
}
