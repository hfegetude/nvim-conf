return {
	{
		"akinsho/toggleterm.nvim",
		config = function()
			require("toggleterm").setup{}
			local Terminal  = require('toggleterm.terminal').Terminal
			Lazygit = Terminal:new({
				cmd = "lazygit",
				dir = "git_dir",
				direction = "float",
			})

			Term1 = Terminal:new({
				dir = "git_dir",
				direction = "float",
			})

			Term2 = Terminal:new({
				dir = "git_dir",
				direction = "float",
			})

			Term3 = Terminal:new({
				dir = "git_dir",
				direction = "float",
			})


			local map = require("helpers.keys").map
			map("n", "<leader>lg", "<cmd>lua Lazygit:toggle()<CR>", "Lazygit")
			map("n", "<leader>t1", "<cmd>lua Term1:toggle()<CR>", "Lazygit")
			map("n", "<leader>t2", "<cmd>lua Term2:toggle()<CR>", "Lazygit")
			map("n", "<leader>t3", "<cmd>lua Term3:toggle()<CR>", "Lazygit")
			map({"n", "t"}, "<M-t>", "<cmd>ToggleTermToggleAll<CR>", "ToggleAll terms")
		end
	}
}
