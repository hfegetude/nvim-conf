return {
	{
		"nvimtools/none-ls.nvim",
		config = function()
			local null_ls = require("null-ls")
			null_ls.setup({
				null_ls.builtins.formatting.stylua,
				null_ls.builtins.formatting.black,
				null_ls.builtins.formatting.clang_format,
				null_ls.builtins.formatting.yamlfmt,

				null_ls.builtins.diagnostics.flake8,
				null_ls.builtins.diagnostics.cppcheck,

			})

			local map = require("helpers.keys").map
			map("v", "<leader>lf", "<CMD>lua FormatSelection()<CR>", "Format Selection")
		end
	}
}
